import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Region,
  Ciudad,
} from '../models';
import {RegionRepository} from '../repositories';

export class RegionCiudadController {
  constructor(
    @repository(RegionRepository)
    public regionRepository: RegionRepository,
  ) { }

  @get('/regions/{id}/ciudad', {
    responses: {
      '200': {
        description: 'Ciudad belonging to Region',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Ciudad)},
          },
        },
      },
    },
  })
  async getCiudad(
    @param.path.number('id') id: typeof Region.prototype.id,
  ): Promise<Ciudad> {
    return this.regionRepository.ciudad(id);
  }
}
