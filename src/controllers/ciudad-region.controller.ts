import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Ciudad,
  Region,
} from '../models';
import {CiudadRepository} from '../repositories';

export class CiudadRegionController {
  constructor(
    @repository(CiudadRepository) protected ciudadRepository: CiudadRepository,
  ) { }

  @get('/ciudads/{id}/regions', {
    responses: {
      '200': {
        description: 'Array of Ciudad has many Region',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Region)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Region>,
  ): Promise<Region[]> {
    return this.ciudadRepository.regions(id).find(filter);
  }

  @post('/ciudads/{id}/regions', {
    responses: {
      '200': {
        description: 'Ciudad model instance',
        content: {'application/json': {schema: getModelSchemaRef(Region)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Ciudad.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Region, {
            title: 'NewRegionInCiudad',
            exclude: ['id'],
            optional: ['ciudadId']
          }),
        },
      },
    }) region: Omit<Region, 'id'>,
  ): Promise<Region> {
    return this.ciudadRepository.regions(id).create(region);
  }

  @patch('/ciudads/{id}/regions', {
    responses: {
      '200': {
        description: 'Ciudad.Region PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Region, {partial: true}),
        },
      },
    })
    region: Partial<Region>,
    @param.query.object('where', getWhereSchemaFor(Region)) where?: Where<Region>,
  ): Promise<Count> {
    return this.ciudadRepository.regions(id).patch(region, where);
  }

  @del('/ciudads/{id}/regions', {
    responses: {
      '200': {
        description: 'Ciudad.Region DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Region)) where?: Where<Region>,
  ): Promise<Count> {
    return this.ciudadRepository.regions(id).delete(where);
  }
}
