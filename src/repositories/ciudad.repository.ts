import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Ciudad, CiudadRelations, Region} from '../models';
import {MysqldbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {RegionRepository} from './region.repository';

export class CiudadRepository extends DefaultCrudRepository<
  Ciudad,
  typeof Ciudad.prototype.id,
  CiudadRelations
> {

  public readonly regions: HasManyRepositoryFactory<Region, typeof Ciudad.prototype.id>;

  constructor(
    @inject('datasources.mysqldb') dataSource: MysqldbDataSource, @repository.getter('RegionRepository') protected regionRepositoryGetter: Getter<RegionRepository>,
  ) {
    super(Ciudad, dataSource);
    this.regions = this.createHasManyRepositoryFactoryFor('regions', regionRepositoryGetter,);
    this.registerInclusionResolver('regions', this.regions.inclusionResolver);
  }
}
