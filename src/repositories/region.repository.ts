import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Region, RegionRelations, Ciudad} from '../models';
import {MysqldbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {CiudadRepository} from './ciudad.repository';

export class RegionRepository extends DefaultCrudRepository<
  Region,
  typeof Region.prototype.id,
  RegionRelations
> {

  public readonly ciudad: BelongsToAccessor<Ciudad, typeof Region.prototype.id>;

  constructor(
    @inject('datasources.mysqldb') dataSource: MysqldbDataSource, @repository.getter('CiudadRepository') protected ciudadRepositoryGetter: Getter<CiudadRepository>,
  ) {
    super(Region, dataSource);
    this.ciudad = this.createBelongsToAccessorFor('ciudad', ciudadRepositoryGetter,);
    this.registerInclusionResolver('ciudad', this.ciudad.inclusionResolver);
  }
}
